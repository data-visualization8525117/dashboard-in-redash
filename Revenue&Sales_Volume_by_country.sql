-- Revenue & Sales Volume by country

SELECT Country, 
       SUM(Quantity * UnitPrice) AS Revenue,
       SUM(Quantity) AS SalesVolume
FROM retail
WHERE Description != 'Manual'        -- filter data on positions removed from the order
    and  Quantity > 0                -- filter data on returned goods
    and Country != 'United Kingdom'  -- filter data by UK
    and InvoiceDate < '2011-12-01'   -- filter data for the last month (incomplete)
GROUP BY Country
ORDER BY Revenue
