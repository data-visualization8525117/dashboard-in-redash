--UK Share

SELECT CASE 
          WHEN Country='United Kingdom' THEN 'United Kingdom'
          ELSE 'Other countries'
          END AS Country,
    SUM(Quantity * UnitPrice) AS Revenue,
    SUM(Quantity) AS SalesVolume
    FROM retail
    WHERE Description != 'Manual' 
        and  Quantity > 0
        
    GROUP BY Country

