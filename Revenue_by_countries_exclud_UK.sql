-- Revenue by countries exclud. UK

WITH t as
(
    SELECT Country, Quantity, UnitPrice
    FROM retail
    WHERE Country != 'United Kingdom'       -- filter data by UK
            and Description != 'Manual'     -- filter data on positions removed from the order
            and  Quantity > 0               -- filter data on returned goods
            and InvoiceDate < '2011-12-01'  -- filter data for the last month (incomplete)
)
-- select top-5 countries
SELECT CASE 
          WHEN Country='EIRE' THEN 'EIRE'
          WHEN Country='Germany' THEN 'Germany'
          WHEN Country='France' THEN 'France'
          WHEN Country='Netherlands' THEN 'Netherlands'
          WHEN Country='Australia' THEN 'Australia'
          ELSE 'Other countries'
          END AS Country,          
    
    SUM(Quantity * UnitPrice) AS Revenue,
    SUM(Quantity) AS SalesVolume
FROM t
    
        
GROUP BY Country
Order by Revenue
