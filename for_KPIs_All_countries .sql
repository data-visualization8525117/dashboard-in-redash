-- for KPIs	All countries 

SELECT 
    SUM(Quantity * UnitPrice) AS TotalRevenue,
    SUM(Quantity) AS TotalSalesVolume
    FROM retail
    WHERE Description != 'Manual'        -- filter data on positions removed from the order
        and  Quantity > 0                -- filter data on returned goods
        and InvoiceDate < '2011-12-01'   -- filter data for the last month 
