-- Average Order Value (AOV) Dec 2010 - Nov 2011 (KPI, all countries)

WITH t1 as 
(
    SELECT InvoiceNo,
    SUM(Quantity * UnitPrice) AS TotalCost, 
    Country
    FROM retail
    WHERE  Description != 'Manual'                   -- filter data on positions removed from the order
    and  Quantity > 0                                -- filter data on returned goods
    and toStartOfMonth(InvoiceDate) != '2011-12-01'  -- filter data for the last month (incomplete)
    GROUP BY Country, InvoiceNo
)

SELECT  
Round(AVG(TotalCost), 2) AS AOV
FROM t1
 
