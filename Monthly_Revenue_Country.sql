-- Monthly Revenue by Country

SELECT toStartOfMonth(InvoiceDate) AS Month, 
       Country,
       SUM(Quantity * UnitPrice) AS Revenue

FROM retail
WHERE Description != 'Manual'         -- filter data on positions removed from the order
    and  Quantity > 0                 -- filter data on returned goods
    and Month != '2011-12-01'         -- filter data for the last month (incomplete)
    and Country ==  '{{ Country }}'   -- create a parameter for the filter
GROUP BY Country, Month 
ORDER BY Month ASC
