-- Sales Volume & Revenue exclud.  UK

SELECT toStartOfMonth(InvoiceDate) AS Month, 
    SUM(Quantity * UnitPrice) AS Revenue,
    SUM(Quantity) AS SalesVolume

FROM retail
WHERE Description != 'Manual'         -- filter data on positions removed from the order
    and  Quantity > 0                 -- filter data on returned goods
    and Month != '2011-12-01'         -- filter data for the last month (incomplete) 
    and Country != 'United Kingdom'   -- filter data by UK
GROUP BY Month
ORDER BY Month

