-- Monthly ARPU by Country

-- find revenue from every customer
WITH t1 as 
(
    SELECT CustomerID, Country,
    toStartOfMonth(InvoiceDate) AS Month, 
    SUM(Quantity * UnitPrice) AS RevenuePerCustomer
    
    FROM retail
    WHERE  Description != 'Manual'    -- filter data on positions removed from the order
    and  Quantity > 0                 -- filter data on returned goods
    and Month != '2011-12-01'         -- filter data for the last month (incomplete)
   
    GROUP BY Month, CustomerID, Country 
)

SELECT  Month, Country,
       AVG(RevenuePerCustomer) AS ARPU


FROM t1
WHERE Country  ==  '{{ Country }}'   -- create a parameter for the filter
GROUP BY Month, Country 
Order by Month ASC

