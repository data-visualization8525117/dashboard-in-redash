-- Monthly MAU by Country

SELECT Country, 
    toStartOfMonth(InvoiceDate) AS Month,
    COUNT(DISTINCT CustomerID) AS MAU  
    
    from retail 
    WHERE Description != 'Manual'          -- filter data on positions removed from the order
        and  Quantity > 0                  -- filter data on returned goods
        and Month != '2011-12-01'          -- filter data for the last month (incomplete)
        and Country  ==  '{{ Country }}'   -- create a parameter for the filter
     
    GROUP BY Country, Month
    Order by Month ASC, MAU ASC
